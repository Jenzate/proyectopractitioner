var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../serverV2');

var should = chai.should();
chai.use(chaiHttp) // configurar chai con modulo http

describe('Pruebas serverV2 back Mlab', () => {
  it('Test BBVA', (done) => {
      chai.request('http://www.bbva.com')
        .get('/')
        .end((err,res) =>{
          //console.log(res),
          res.should.have.status(200)
          //res.header['x-amz-cf-id']=='2KS33UieSrw5VCfU1102MDi61CyqY883dxssChsBwrWFT9uuXCy8ng=='
          done();
        })

  });
  it('Test FIFA', (done) => {
      chai.request('http://www.fifa.com')
        .get('/')
        .end((err,res) =>{
          //console.log(res),
          res.should.have.status(200)
          //res.header['x-amz-cf-id']=='2KS33UieSrw5VCfU1102MDi61CyqY883dxssChsBwrWFT9uuXCy8ng=='
          done();
        })

  });
  it('Mi APP Funciona', (done) => {
      chai.request('http://localhost:3000')
        .get('/colapi/V3/users/')
        .end((err,res) =>{
          //console.log(res),
          res.should.have.status(200)
          done();
        })

  });
  it('Arreglo de usuarios', (done) => {
      chai.request('http://localhost:3000')
        .get('/colapi/V3/users/')
        .end((err,res) =>{
          //console.log(res.body)
          res.body.should.be.a('array'),
          //res.should.have.status(200)
          done();
        })

  });
  it('Tiene elementos', (done) => {
      chai.request('http://localhost:3000')
        .get('/colapi/V3/users/')
        .end((err,res) =>{
          //console.log(res.body)
          res.body.length.should.be.gte(1),
          //res.should.have.status(200)
          done();
        })

  });
  //it('validar elementos', (done) => {
//      chai.request('http://localhost:3000')
//        .get('/colapi/V3/users/')
  //      .end((err,res) =>{
  //        console.log(res.body[0])
  //        res.body[0].should.have.property('id,')
  //        res.body[0].should.have.property('first_name,')
  //        res.body[0].should.have.property('last_name,'),
  //        //res.should.have.status(200)
  //        done();
  //      })

  //});
  //it('insertar usuario', (done) => {
  //    chai.request('http://localhost:3000')
  //      .post('/colapi/V3/users/')
  //      .send('{"id":"24","first_name":"jennifer","last_name":"zate"}')
  //      .end((err,res, body) =>{
  //          console.log(body);
  //          body.should.be.eql('')
  //        done();
  //      })

//  });

});
