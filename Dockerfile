# imagen inicial a partir de la cual creamos nuestra imagen
FROM node

# definir directorio del contenerdor
WORKDIR /colapidb_jaa

# añadimos contenido del proyecto en directorio de contenedor
ADD . /colapidb_jaa

# puerto de escucha del contenedor, debe ser el mismo de la aplicacion
EXPOSE 3000

# Comandos para lanzar nuestra API REST 'colapi'
# si es en ambiente productivo seria CMD ["node", "server.js"]
CMD ["npm", "start"]
