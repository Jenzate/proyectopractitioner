// CRUD con repositorio mongo accedido a traves de Mlbab

var express = require('express');
var bodyParser = require('body-parser');//verificar si llega un json en el body
var requestJson = require('request-json');
var app = express();
var port = process.env.PORT || 3000;
var usersFile = require('./user.json');
var URLbase = "/colapi/V3/";
var baseMLabURL = "https://api.mlab.com/api/1/databases/colapidb_jaa/collections/";
var apiKey = "apiKey=18ZS4o9NEsF82rr5Y--4CeRRbtkqnfE7";
var queryString = 'f={"_id":0}&';

app.listen(port);
app.use(bodyParser.json());         // bodyParser libreria node_modules

console.log("COLAPI v3, serverV2 escuchando por el puerto"+port+ "...");

//---------------------- Modulo Usuarios------------------------------------

// GET generica recupera todos los usuarios
// URL localhost:3000/colapi/V3/users/

app.get(URLbase + 'users',
function(req, res) {
  console.log("clientes HTTP mLab recuperados");
  var httpClient = requestJson.createClient(baseMLabURL); // instancia la clase requestjson y a traves del metodo createClient realiza la consulta a bd mongp usando Mlab
  httpClient.get('user?'+queryString+ apiKey,
    function(err, respuestaMlab, body){
      console.log("err: "+err);

      var respuesta ={};
      respuesta = !err ? body: {"msg" : "Error al recuperar user de mLab"}
      res.send(respuesta)
  });

});

// ------- LOGIN de un usuario enviado por parametro desde el front ----
// URL localhost:3000/colapi/V3/login/

app.post(URLbase + 'login',
function(req, res) {
  clienteMlab = requestJson.createClient(baseMLabURL)
  var mailrec = req.body.email;
  var claverec = req.body.password;
  var encontrado = false;
  var queryString = 'q={"email":' + '"' + mailrec + '"' + '}&';
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get('user?' + queryString + apiKey,
     function(err,respuestaMlab,body){
           var respGet=body[0];
           console.log("queryString = " + queryString);
           console.log ("email= " + mailrec);
           console.log("respuesta mLab get DE VALIDACION correcta" );
           //var respuesta = {};
           console.log("body = " + JSON.stringify(req.body));
           if (body.length == 0) {
             console.log("usuario NO existe= " + req.body.email);
             var respuesta = {"msg": "usuario no logueado"};
             res.send(respuesta);
           }
           else
           {
             console.log("usuario SI existe= " + req.body.email);
             console.log("body.password = " + body[0].password);
             console.log("claverec = " + claverec);
             if (body[0].password == claverec)
             {

               console.log("password correcto para el id= " + body[0].id);

               clienteMlabP = requestJson.createClient(baseMLabURL  + "/user");

               var logged = {"logged": true};
               var cambio = '{"$set":' + JSON.stringify(logged) + '}';


               clienteMlabP.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio),
                function(err, resM, body) {
                  var respuesta = {};
                  console.log("el id es: "+ JSON.stringify(respGet));

                  respuesta = !err ? {"msg": "usuario logueado", "id": respGet.id}: body
                  console.log( respuesta );
                  res.send(respuesta);
               });
             }
             else
             {
               console.log("password INVALIDO para el id= " + body[0].id);
               res.send({"msg":"usuario no logueado"});
             }
           }
       });
  });


// ------- LOGOUT de un usuario enviado por parametro desde el front ----
// URL localhost:3000/colapi/V3/logout/
app.post(URLbase + 'logout/',
 function(req, res) {
   console.log("GET /Colapi/v3/logout/");
   console.log("desde front"+req.body);
   var email = req.body.email;
   var password = req.body.password;
   var httpClient= requestJson.createClient(baseMLabURL);
   var queryStringid = 'q={"email":'+'"'+email+'"'+'}&';

   console.log(" query: "+ queryStringid);

   // obtener los datos de mLab del usuario enviado desde el front
   httpClient.get('user?'+queryString +queryStringid + apiKey,
    function(err, respuestaMlab, body){
      console.log("respuesta Mlab para get correcta.. err: "+err+" respuesta: " +respuestaMlab+" Body:"+ body[0]);
      respuesta = body[0];

      if (body.length > 0){
        console.log("longitud: "+body.length);
        console.log("body.password: "+ body[0].password);


              clienteMlab = requestJson.createClient(baseMLabURL + "/user");
              var log = {"logged":true};                                // asignar la propiedad al usuario
              var cambio = '{"$unset":' + JSON.stringify(log) + '}'     // pasarla a string

              clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), //agregar la propiedad al usuario
              function(err, resM, body) {
                var respuesta ={};
                respuesta = !err ? {"msg":"sesion finalizada"}: body
                res.send(respuesta)
              });

        }else{res.send({"msg":"No se pudo cerrar la conexion"});}
    });
});

// ------- GET de un solo usuario que se pasa por parametro desde el front ----
// URL localhost:3000/colapi/V3/users/1

app.get(URLbase + 'users/:id',
 function (req, res) {
   console.log("GET /Colapi/v3/users/:id");
   console.log(req.params.id);
   var id = req.params.id;
   var queryStringid = 'q={"id":'+id+'}&';
   var httpClient= requestJson.createClient(baseMLabURL);
   httpClient.get('user?'+queryString +queryStringid + apiKey,
    function(err, respuestaMlab, body){
      console.log("respuesta Mlab correcta");
      var respuesta = body[0];
      res.send(respuesta);
    });

 });

//-- Insertar un nuevo usuario recuperando los datos del front a traves del body
// URL localhost:3000/colapi/V3/users/ y  en el body se envian los datos

 app.post(URLbase + 'users/',
  function(req, res) {
    clienteMlab = requestJson.createClient(baseMLabURL + "/user?" + apiKey)
    clienteMlab.post('', req.body,
      function(err, resM, body) {
          res.send(body)
      });
});

// Modificar un usuario rrecuperando los datos desde del front a traves del body
// URL localhost:3000/colapi/V3/users/21

 app.put(URLbase + 'users/:id',
  function(req, res) {
    clienteMlab = requestJson.createClient(baseMLabURL + "/user")
    var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
    clienteMlab.put('?q={"id": ' + req.params.id + '}&' + apiKey, JSON.parse(cambio),
    function(err, resM, body) {
      res.send(body)
  });
});

//-------------------Modulo Cuentas------------------------------------

// ---------  GET generica recupera todas las cuentas ------------
// URL localhost:3000/colapi/V3/cuentas/

app.get(URLbase + 'cuentas',
function(req, res) {
  console.log("consulta cuenta HTTP mLab Creado");
  var httpClient = requestJson.createClient(baseMLabURL); // instancia la clase requestjson y a traves del metodo createClient realiza la consulta a bd mongp usando Mlab
  httpClient.get('cuenta?'+queryString+ apiKey,
    function(err, respuestaMlab, body){
      console.log("err: "+err);
      console.log("respuestaMlab: "+respuestaMlab);
      console.log("body:"+body);

      var respuesta ={};
      respuesta = !err ? body: {"msg" : "Error al recuperar user de mLab"}
      res.send(respuesta)
  });

});

// ---- GET de una sola cuenta que se pasa por parametro desde el front ------

// EN EL SENTIDO ESTRICTO SI HUBIERA INTEGRIDAD REFERENCIAL DE MODELO DE BD
// la query seria user/:id/cuenta/:iban/movimiento
//URL localhost:3000/colapi/V3/users/3/cuentas/

app.get(URLbase + 'users/:user_id/cuentas/',
 function (req, res) {
   console.log("GET /Colapi/v3/cuentas/:user_id");
   console.log(req.params.user_id);
   var user_id = req.params.user_id;
   var queryStringid = 'q={"user_id":'+user_id+'}&';
   var httpClient= requestJson.createClient(baseMLabURL);
   httpClient.get('cuenta?'+queryString +queryStringid + apiKey,
    function(err, respuestaMlab, body){
      console.log("respuesta Mlab correcta para cuentas: "+ JSON.stringify(body));
      var respuesta = body;
      res.send(respuesta);
    });

 });

 //--------------------Modulo Movimientos------------------------------------
// URL localhost:3000/colapi/V3/movimientos/

 // ---------- GET generica recupera todas las cuentas -----------------
 app.get(URLbase + 'movimientos',
 function(req, res) {
   console.log("consulta movimientos HTTP mLab Creado");
   var httpClient = requestJson.createClient(baseMLabURL); // instancia la clase requestjson y a traves del metodo createClient realiza la consulta a bd mongp usando Mlab
   httpClient.get('movimiento?'+queryString+ apiKey,
     function(err, respuestaMlab, body){
       console.log("err: "+err);
       console.log("respuestaMlab movtos: "+respuestaMlab);
       console.log("body:"+body);

       var respuesta ={};
       respuesta = !err ? body: {"msg" : "Error al recuperar movtos de mLab"}
       res.send(respuesta)
   });

 });

 // ------ GET de una sola cuenta que se pasa por parametro desde el front ----
// URL localhost:3000/colapi/V3/movimientos/"LV46 JGEI IM3Z OAIL O7DD H"
 app.get(URLbase + 'movimientos/:iban',
  function (req, res) {
    console.log("GET /Colapi/v3/movimientos/:iban");
    console.log(req.params.iban);
    var iban= req.params.iban;
    var queryStringid = 'q={"iban":'+iban+'}&';
    var httpClient= requestJson.createClient(baseMLabURL);
    httpClient.get('movimiento?'+queryString +queryStringid + apiKey,
     function(err, respuestaMlab, body){
       console.log("respuesta Mlab correcta movtos por iban"+ JSON.stringify(body));
       var respuesta = body;
       res.send(respuesta);

     });

  });

  // ------ GET de una sola cuenta que se pasa por parametro desde el front ----
  // URL localhost:3000/colapi/V3/users/2/cuentas/"LV46 JGEI IM3Z OAIL O7DD H"/movimientos/

  app.get(URLbase + 'users/:id/cuentas/:iban/movimientos',
   function (req, res) {
     console.log("GET /Colapi/v3/movimientos/:iban");
     console.log(req.params.iban);
     var iban= req.params.iban;
     var queryStringid = 'q={"iban":'+iban+'}&';
     var httpClient= requestJson.createClient(baseMLabURL);
     httpClient.get('movimiento?'+queryString +queryStringid + apiKey,
      function(err, respuestaMlab, body){
        console.log("respuesta Mlab correcta");
        var respuesta = body;
        res.send(respuesta);
      });

   });


 // ------ GET de una sola cuenta que se pasa por parametro desde el front ----
 // localhost:3000/colapi/V3/users/2/cuentas/"LV46 JGEI IM3Z OAIL O7DD H"/movimientos/
 app.get(URLbase + 'users/:id/cuentas/:iban/movimientos',
  function (req, res) {
    console.log("GET /Colapi/v3/movimientos/:iban");
    console.log(req.params.iban);
    var iban= req.params.iban;
    var queryStringid = 'q={"iban":'+iban+'}&';
    var httpClient= requestJson.createClient(baseMLabURL);
    httpClient.get('movimiento?'+queryString +queryStringid + apiKey,
     function(err, respuestaMlab, body){
       console.log("respuesta Mlab correcta");
       var respuesta = body;
       res.send(respuesta);
     });

  });
